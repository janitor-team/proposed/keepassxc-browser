Files removed from upstream release
###################################

For the Debian package the following files are removed from the upstream
distribution (see Files-Excluded field in debian/copyright):

Upstream                                          | Replacement (file or packagename)
=======================================================================
.github/                                          | -
.tx/                                              | -
keepassxc-browser/background/nacl-util.min.js     | d/missing-sources/nacl-utils.js
keepassxc-browser/background/nacl.min.js          | node-tweetnacl
keepassxc-browser/bootstrap/bootstrap.min.css     | libjs-bootstrap4
keepassxc-browser/bootstrap/bootstrap.min.js      | libjs-bootstrap4
keepassxc-browser/bootstrap/jquery-3.4.1.min.js   | node-jquery
keepassxc-browser/common/browser-polyfill.min.js  | d/missing-sources/browser-polyfill.js
keepassxc-browser/fonts/fork-awesome.min.css      | fonts-fork-awesome
keepassxc-browser/fonts/forkawesome-webfont.woff2 | fonts-fork-awesome

NOTE: The above list is to be kept in sync with Files-Excluded in
debian/copyright! Rationale: The above list also documents replacements.

To download replacement files to debian/missing-sources/, adapt and run
debian/fetch-missing-sources.sh.

I decided to install human-readable files in the binary package and patch files
that include them instead.
Rationale: Since I am not experienced with web software and this package is
about to go through the NEW packages queue, I want to have as little
optimization as possible. In case of performance issues, or avoidable extra
maintainer effort, or if it simply turns out it is safe to include optimized
files, I am fine to change this later at any time.

Updating the package with new upstream release
##############################################
1. Use uscan to download the latest upstream release
2. Check in the repacked orig.tar.xz that no font files and no *.min.* files are
   included.
3. Update debian/changelog, build, etc.

Obtain extension key from Chromium extensions repository
########################################################
When the extension is executed in Chromium it uses the key 'key' in
keepassxc-browser/manifest.json to compute its identity it reports to KeePassXC,
which keeps an compiled-in whitelist for permitted IDs.

This key is generated once when an extension is uploaded to the Chromium
extensions repository for the first time and added automatically to
manifest.json.

We add this key to keepassxc-browser/manifest.json by
debian/patches/chromium-extension-key.patch. The value must be obtained manually
by the maintainer with these steps:
1. Deinstall webext-keepassxc-browser
2. Start Chromium
3. Install the KeePassXC-Browser extension with Chromium
4. The key is the value of the key 'key' in
   $HOME/.config/chromium/Extensions/<ID>/<VERSION>/manifest.json

See also:
https://stackoverflow.com/questions/21497781/how-to-change-chrome-packaged-app-id-or-why-do-we-need-key-field-in-the-manifest
